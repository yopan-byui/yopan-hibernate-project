package com.yopan.hibernate;

import javax.persistence.*;

@Entity
@Table(name = "players", schema = "java_hibernate")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "level")
    private int level;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
    public String toString() {
        return "Id: " + Integer.toString(id) + " - Player: " + name + " - LeveL: " + level;
    }
}