package com.yopan.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class Query {

    SessionFactory factory = null;
    Session session = null;

    private static Query single_instance = null;

    private Query()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    public static Query getInstance()
    {
        if (single_instance == null) {
            single_instance = new Query();
        }

        return single_instance;
    }
    public List<Player> getCustomers() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.yopan.hibernate.Player";
            List<Player> players = (List<Player>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return players;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Player getCustomer(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.yopan.hibernate.Player where id=" + Integer.toString(id);
            Player player = (Player)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return player;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Player saveCustomer(String name, int level) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();

            Player player = new Player();
            player.setName(name);
            player.setLevel(level);

            session.save(player);

            session.getTransaction().commit();

            return player;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
