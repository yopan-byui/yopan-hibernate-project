package com.yopan.hibernate;

import java.util.*;
import java.util.regex.*;

public class RunHibernateQueries {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Query query = Query.getInstance();

        boolean continueRunning = true;

        do {
            System.out.println("Create new player");
            System.out.println("Or press '0' to LEAVE and List current Players!");

            String newUserName = getUserName(scanner);

            if(newUserName == "0") {
                continueRunning = false;
                System.out.println("Thanks for using our app. Good-bye");
            } else {
                int newUserLevel = getUserLevel(scanner);

                try {
                    Player newPlayerCreated = query.saveCustomer(newUserName, newUserLevel);

                    System.out.println("New player created successfully");
                    System.out.println(newPlayerCreated);
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }

        } while (continueRunning);

        printList (query);

        int playerId = 1;

        printPlayerById (query, playerId);

    }

    public static String getUserName(Scanner scanner) {
        boolean invalidInputFormat = true;
        String input;
        String newPlayerName = null;
        System.out.print("Please provide the new player's name: ");
        do {
            input = scanner.nextLine();
            if(input.equals("0")) {
                return "0";
            } else if(isNameValid(input)) {
                invalidInputFormat = false;
                newPlayerName = input;
            } else {
                System.out.println("That was not a valid name. Please try again.");
            }

        } while (invalidInputFormat);

        return newPlayerName;
    }

    public static int getUserLevel(Scanner scanner) {
        boolean invalidInputFormat = true;
        String input;
        int newPlayerLevel = 0;
        System.out.print("Please provide the new player's level: ");
        do {
            input = scanner.nextLine();
            if(isLevelValid(input)) {
                invalidInputFormat = false;
                newPlayerLevel = Integer.parseInt(input);
            } else {
                System.out.println("That was not a valid level. Please try again.");
            }

        } while (invalidInputFormat);

        return newPlayerLevel;
    }

    public static void printList (Query query) {
        System.out.println("List of all registered players:");

        try {
            List<Player> players = query.getCustomers();
            for (Player player : players) {
                System.out.println(player);
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public static void printPlayerById (Query query, int playerId) {
        System.out.println("Details of player 01:");

        try {
            System.out.println(query.getCustomer(1));
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public static boolean isNameValid(String input) {
        String regex = "(?i)(^[a-z])((?![ .,'-]$)[a-z .,'-]){0,24}$";
        Pattern pattern = Pattern.compile(regex);

        if (input == null) {
            return false;
        }

        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    public static boolean isLevelValid (String input) {
        try {
            Integer.parseInt(input);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}
